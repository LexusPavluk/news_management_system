DROP TABLE IF EXISTS news CASCADE;
DROP TABLE IF EXISTS comments;

CREATE TABLE news
(
    news_id    BIGSERIAL PRIMARY KEY,
    news_date  DATE                NOT NULL,
    news_title VARCHAR(255) UNIQUE NOT NULL,
    news_text  TEXT                NOT NULL
);

CREATE TABLE comments
(
    comments_id       BIGSERIAL PRIMARY KEY,
    comments_date     TIMESTAMP   NOT NULL,
    comments_text     TEXT        NOT NULL,
    comments_username VARCHAR(25) NOT NULL,
    comments_news_id  BIGINT      NOT NULL REFERENCES news (news_id) ON DELETE CASCADE,
    FOREIGN KEY (comments_news_id) REFERENCES news (news_id)
);