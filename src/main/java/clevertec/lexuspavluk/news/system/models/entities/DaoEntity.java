package clevertec.lexuspavluk.news.system.models.entities;

import java.io.Serializable;

/**
 * Marker interface for entities.
 * @author Alexey Pavliuchenkov 09.04.2022
 */
public interface DaoEntity extends Serializable {

    Long getId();

    void setId(Long id);
}
