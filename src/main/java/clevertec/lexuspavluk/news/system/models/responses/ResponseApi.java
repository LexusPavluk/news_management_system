package clevertec.lexuspavluk.news.system.models.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */
@Data
@AllArgsConstructor
public class ResponseApi<T> {

    /**
     * Response data
     */
    private T data;
}
