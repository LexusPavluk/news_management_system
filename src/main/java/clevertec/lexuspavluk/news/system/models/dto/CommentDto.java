package clevertec.lexuspavluk.news.system.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Data
@NoArgsConstructor
public class CommentDto implements Dto {

    private Long id;
    private Date date;
    private String username;
    private String text;
    private Long news_id;

}
