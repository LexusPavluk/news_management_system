package clevertec.lexuspavluk.news.system.models.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Data
@NoArgsConstructor
public class NewsDto implements Dto {

    private Long id;
    private Date date;
    private String title;
    private String text;
    @EqualsAndHashCode.Exclude
    private List<CommentDto> comments;
}
