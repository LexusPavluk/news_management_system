package clevertec.lexuspavluk.news.system.models.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "comments")
public class Comment implements DaoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comments_id", nullable = false)
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "comments_date")
    private Date date;

    @Column(name = "comments_username")
    private String username;

    @Column(name = "comments_text")
    private String text;

    @Column(name = "comments_news_id")
    private Long news_id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Comment comment = (Comment) o;
        if (!date.equals(comment.date)) return false;
        if (!news_id.equals(comment.news_id)) return false;
        if (!text.equals(comment.text)) return false;
        return username.equals(comment.username);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + news_id.hashCode();
        result = 31 * result + text.hashCode();
        return result;
    }
}
