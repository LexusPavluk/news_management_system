package clevertec.lexuspavluk.news.system.models.dto;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */


public interface Dto {

    Long getId();

    void setId(Long id);

}
