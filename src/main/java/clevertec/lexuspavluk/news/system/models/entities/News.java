package clevertec.lexuspavluk.news.system.models.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "news")
public class News implements DaoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "news_id", nullable = false)
    private Long id;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "news_date")
    private Date date;

    @Column(name = "news_title")
    private String title;

    @Column(name = "news_text")
    private String text;

    @OneToMany(mappedBy = "news_id", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Comment> comments = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        News news = (News) o;
        if (!date.equals(news.date)) return false;
        if (!title.equals(news.title)) return false;
        return text.equals(news.text);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + text.hashCode();
        return result;
    }
}
