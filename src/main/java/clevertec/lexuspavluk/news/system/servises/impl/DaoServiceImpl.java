package clevertec.lexuspavluk.news.system.servises.impl;

import clevertec.lexuspavluk.news.system.aspects.LoggedMethod;
import clevertec.lexuspavluk.news.system.models.dto.Dto;
import clevertec.lexuspavluk.news.system.models.entities.DaoEntity;
import clevertec.lexuspavluk.news.system.servises.DaoService;
import clevertec.lexuspavluk.news.system.utils.mappers.IMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public abstract class DaoServiceImpl<E extends DaoEntity, D extends Dto> implements DaoService<D> {

    private final JpaRepository<E, Long> repository;
    private final IMapper<E, D> mapper;

    @Override
    public List<D> getAllEntities(int page, int pageSize) {
        return repository.findAll(PageRequest.of(page, pageSize, Sort.by("date"))).stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<D> get(long id) {
        return Optional.of(mapper.toDto(repository.getById(id)));
    }

    @Override
    public Optional<D> post(D dto) {
        E entity = mapper.toEntity(dto);
        entity = repository.saveAndFlush(entity);
        return Optional.of(mapper.toDto(entity));
    }

    @LoggedMethod
    @Override
    public void delete(D dto) {
        repository.delete(mapper.toEntity(dto));
        repository.flush();
    }
}
