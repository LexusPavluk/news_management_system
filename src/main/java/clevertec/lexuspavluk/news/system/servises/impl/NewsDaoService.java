package clevertec.lexuspavluk.news.system.servises.impl;

import clevertec.lexuspavluk.news.system.models.dto.NewsDto;
import clevertec.lexuspavluk.news.system.models.entities.News;
import clevertec.lexuspavluk.news.system.repositories.NewsRepository;
import clevertec.lexuspavluk.news.system.utils.mappers.IMapper;
import org.springframework.stereotype.Service;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Service
public class NewsDaoService extends DaoServiceImpl<News, NewsDto> {

    public NewsDaoService(NewsRepository repository, IMapper<News, NewsDto> mapper) {
        super(repository, mapper);
    }
}
