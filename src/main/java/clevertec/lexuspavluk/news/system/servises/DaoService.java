package clevertec.lexuspavluk.news.system.servises;

import java.util.List;
import java.util.Optional;


/**
 * General interface of the Dao-Service layer.
 *
 * @param <T> type of DTO-class.
 *
 * @author Alexey Pavliuchenkov 09.04.2022
 *
 */

public interface DaoService<T> {

    /**
     * Get all dto type T from DAO-layer.
     *
     * @return List type T objects.
     */
    List<T> getAllEntities(int page, int pageSize);

    /**
     * Create new record in DAO-layer.
     *
     * @param dto - saving object.
     * @return confirmation object store in DAO-layer.
     */
    Optional<T> post(T dto);

    /**
     * Read record from DAO-layer, appropriate specified identifier.
     *
     * @param id - identifier,
     * @return - object T-type, appropriate specified identifier.
     */
    Optional<T> get(long id);

    /**
     * Delete record from DAO-layer, appropriate specified identifier.
     *
     * @param dto - dto for delete,
     */
    void delete(T dto);

}
