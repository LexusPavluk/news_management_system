package clevertec.lexuspavluk.news.system.servises.impl;

import clevertec.lexuspavluk.news.system.models.dto.CommentDto;
import clevertec.lexuspavluk.news.system.models.entities.Comment;
import clevertec.lexuspavluk.news.system.repositories.CommentsRepository;
import clevertec.lexuspavluk.news.system.utils.mappers.IMapper;
import org.springframework.stereotype.Service;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Service
public class CommentsDaoService extends DaoServiceImpl <Comment, CommentDto> {

    public CommentsDaoService(CommentsRepository repository, IMapper<Comment, CommentDto> mapper) {
        super(repository, mapper);
    }
}
