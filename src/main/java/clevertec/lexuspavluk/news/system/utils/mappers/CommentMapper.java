package clevertec.lexuspavluk.news.system.utils.mappers;

import clevertec.lexuspavluk.news.system.models.dto.CommentDto;
import clevertec.lexuspavluk.news.system.models.entities.Comment;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */
@Mapper(componentModel = "spring")
public abstract class CommentMapper implements IMapper<Comment, CommentDto> {

    @Override
    public abstract CommentDto toDto(Comment entity);

    @Override
    @InheritInverseConfiguration
    public abstract Comment toEntity(CommentDto dto);
}
