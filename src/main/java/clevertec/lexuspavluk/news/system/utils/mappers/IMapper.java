package clevertec.lexuspavluk.news.system.utils.mappers;

import clevertec.lexuspavluk.news.system.models.dto.Dto;
import clevertec.lexuspavluk.news.system.models.entities.DaoEntity;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

public interface IMapper<S extends DaoEntity, T extends Dto> {

    T toDto(S entity);

    S toEntity(T dto);
}
