package clevertec.lexuspavluk.news.system.utils.mappers;

import clevertec.lexuspavluk.news.system.models.dto.NewsDto;
import clevertec.lexuspavluk.news.system.models.entities.News;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */
@Mapper(componentModel = "spring")
public abstract class NewsMapper implements IMapper<News, NewsDto> {

    @Override
    public abstract NewsDto toDto(News entity);

    @Override
    @InheritInverseConfiguration
    public abstract News toEntity(NewsDto dto);
}
