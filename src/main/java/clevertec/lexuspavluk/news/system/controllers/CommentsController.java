package clevertec.lexuspavluk.news.system.controllers;

import clevertec.lexuspavluk.news.system.models.dto.CommentDto;
import clevertec.lexuspavluk.news.system.servises.impl.CommentsDaoService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */
@Controller
@RequestMapping("/news/{number:\\d+}/comment")
public class CommentsController extends CrudControllerAssistant<CommentDto> {

    public CommentsController(CommentsDaoService daoService) {
        super(daoService);
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<CommentDto> getDto(@PathVariable long id) {
        return super.getDto(id);
    }

    @Override
    @GetMapping("/all/{page}/{pageSize}")
    public ResponseEntity<List<CommentDto>> getAllDto(@PathVariable int page,
                                                                      @PathVariable int pageSize) {
        return super.getAllDto(page, pageSize);
    }

    @Override
    @PostMapping
    public ResponseEntity<CommentDto> postDto(@RequestBody CommentDto dto) {
        return super.postDto(dto);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<String> deleteDto(@RequestBody CommentDto dto) {
        return super.deleteDto(dto);
    }
}
