package clevertec.lexuspavluk.news.system.controllers;

import clevertec.lexuspavluk.news.system.models.dto.Dto;
import clevertec.lexuspavluk.news.system.servises.DaoService;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Common class with implementation mostly-common HTML-methods.
 *
 * @author Alexey Pavliuchenkov 09.04.2022
 */
public class CrudControllerAssistant<T extends Dto> {

    protected final DaoService<T> daoService;

    public CrudControllerAssistant(DaoService<T> daoService) {
        this.daoService = daoService;
    }

    protected ResponseEntity<T> getDto(long id) {
        return daoService.get(id)
                .map(t -> ResponseEntity.ok()
                        .body(t))
                .orElse(ResponseEntity.notFound().build());
    }

    protected ResponseEntity<List<T>> getAllDto(int page, int pageSize) {
        List<T> allEntities = daoService.getAllEntities(page, pageSize);
        if (allEntities.isEmpty())
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok().body(allEntities);
    }

    protected ResponseEntity<T> postDto(T dto) {
        return daoService.post(dto)
                .map(t -> ResponseEntity.ok().body(t))
                .orElse(ResponseEntity.unprocessableEntity().build());
    }

    protected ResponseEntity<String> deleteDto(T dto) {
        try {
            daoService.delete(dto);
            return ResponseEntity.accepted().build();
        } catch (RuntimeException e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

}
