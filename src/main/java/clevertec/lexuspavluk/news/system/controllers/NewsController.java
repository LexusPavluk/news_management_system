package clevertec.lexuspavluk.news.system.controllers;

import clevertec.lexuspavluk.news.system.aspects.LoggedMethod;
import clevertec.lexuspavluk.news.system.models.dto.NewsDto;
import clevertec.lexuspavluk.news.system.servises.impl.NewsDaoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */
@Controller
@RequestMapping("/news")
public class NewsController extends CrudControllerAssistant<NewsDto> {

    public NewsController(NewsDaoService newsDaoService) {
        super(newsDaoService);
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<NewsDto> getDto(@PathVariable long id) {
        return super.getDto(id);
    }

    @Override
    @GetMapping("/all/{page}/{pageSize}")
    public ResponseEntity<List<NewsDto>> getAllDto(@PathVariable int page,
                                                                   @PathVariable int pageSize) {
        return super.getAllDto(page, pageSize);
    }

    @LoggedMethod
    @Override
    @PostMapping
    public ResponseEntity<NewsDto> postDto(@RequestBody NewsDto dto) {
        return super.postDto(dto);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<String> deleteDto(@RequestBody NewsDto dto) {
        return super.deleteDto(dto);
    }
}
