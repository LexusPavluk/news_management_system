package clevertec.lexuspavluk.news.system.repositories;

import clevertec.lexuspavluk.news.system.models.entities.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {

}
