package clevertec.lexuspavluk.news.system.repositories;

import clevertec.lexuspavluk.news.system.models.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Alexey Pavliuchenkov 09.04.2022
 */

@Repository
public interface CommentsRepository extends JpaRepository<Comment, Long> {

}
