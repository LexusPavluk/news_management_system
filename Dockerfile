FROM java:jre

LABEL maintaner="@lexuspavluk"

ADD build/libs/news_management_system-0.0.1-plain.jar /bin/news_management_system.jar

ENTRYPOINT java -jar /bin/news_management_system.jar